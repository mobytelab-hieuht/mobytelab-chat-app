module.exports = (io) => {
  // Set socket.io listenners
  io.on('connection', (socket) => {
    console.log('A user connected');
    // Join a conversation
    socket.on('join-conversation', (conversation) => {
      socket.join(conversation);
      console.log(`Joined conversation: ${conversation}`);
    });
    // Leave a conversation
    socket.on('leave-conversation', (conversation) => {
      socket.leave(conversation);
      console.log(`Left conversation: ${conversation}`);
    });

    socket.on('new-message', (conversation) => {
      io.sockets.in(conversation).emit('refresh-message', conversation);
      console.log(`New message: ${conversation}`);
    });
    socket.on('typing', (data) => {
      socket.broadcast.to(data.conversation).emit('typing-bc', data.user);
      console.log('User is typing...');
    });
    socket.on('stop-typing', (data) => {
      socket.broadcast.to(data.conversation).emit('stop-typing-bc', data.user);
      console.log('User is stop typing...');
    });
    socket.on('disconnect', () => {
      console.log('User disconnected');
    });
  });
};
