const app = require('express')();
const SocketIO = require('socket.io');

const config = require('../config');
const socketEvents = require('./socket_events');

const port = config.io.port || 5000;

// Run SocketIO server
const server = app.listen(port, (err) => {
  if (err) throw err;
  console.log(`SocketIO server listening on port ${port}...`);
});

const io = new SocketIO(server);
socketEvents(io);
