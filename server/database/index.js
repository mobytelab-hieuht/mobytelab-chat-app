const mongoose = require('mongoose');
const config = require('../../config');

// Connect Database
mongoose.connect(config.dbConnectionString, {
  useMongoClient: true,
});

// Connection events
mongoose.connection.on('connected', () => {
  console.log('Database connected');
});
mongoose.connection.on('error', (err) => {
  console.log(`Database connection error: ${err}`);
});
mongoose.connection.on('disconnected', () => {
  console.log('Database disconnected');
});
