const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');

const config = require('../config');
const routesApi = require('./api/routes');

const port = config.server.PORT || 3000;
const app = express();

app.use('/assets', express.static(`${__dirname}/public`));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api', routesApi);
app.use(passport.initialize());

// Connect Database
require('./database');

// Run server
app.listen(port, () => {
  console.log(`App listening on port ${port} ...`);
});
