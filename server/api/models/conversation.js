const mongoose = require('mongoose');

const conversationSchema = mongoose.Schema({
  title: { type: String, default: null, trim: true },
  createAt: { type: String, default: null },
  updateAt: { type: String, default: null },
  participants: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  messages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Message' }], // Hmm! Still thinking about that..
});

module.exports = mongoose.model('Conversation', conversationSchema);
