const mongoose = require('mongoose');

const MessageSchema = mongoose.Schema({
  text: {
    type: String,
    default: null,
    trim: true,
  },
  timeStamp: {
    type: String,
    default: null,
  },
  sender: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Message', MessageSchema);
