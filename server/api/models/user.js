const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const DEFAULT_USER_PHOTO = '/images/default-user-photo.jpg';

const UserSchema = mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true,
  },
  password: { type: String, required: true },
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true,
  },
  fullname: { type: String, default: '' },
  photoUrl: {
    type: String,
    default: DEFAULT_USER_PHOTO,
  },
  conversations: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Conversation' }],
  salt: String,
});

// Set hash password
UserSchema.methods.setPassword = (password) => {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.password = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

// Check valid password
UserSchema.methods.validPassword = (password) => {
  const hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
  return this.password === hash;
};

// General JWT (Json Web Token)
UserSchema.methods.generateJwt = () => {
  const expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign(
    {
      _id: this._id, // eslint-disable-line
      username: this.username,
      email: this.email,
      exp: parseInt(expiry.getTime() / 1000, 10),
    },
    'SECRET',
  );
};

module.exports = mongoose.model('User', UserSchema);
