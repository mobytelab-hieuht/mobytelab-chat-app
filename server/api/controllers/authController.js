const mongoose = require('mongoose');
const passport = require('passport');

const User = mongoose.model('User');

const sendJsonResponse = (res, status, content) => { // eslint-disable-line
  res.status(status);
  res.json(content);
};

// Register new User
module.exports.register = (req, res) => {
  const user = new User();

  user.username = req.body.username;
  user.password = user.generateHash(req.body.password);
  user.email = req.body.email;
  user.fullname = req.body.username;

  user.save((err) => {
    if (err) {
      res.status(400);
      res.json({
        message: 'Can not save user',
      });
    }
    const token = user.generateJwt();
    res.status(200);
    res.json({
      Token: token,
    });
    console.log('A new user created!');
  });
};

// Login
module.exports.login = (req, res) => {
  passport.authenticate('local', (err, user, info) => {
    // Throw error
    if (err) {
      res.status(404).json(err);
      return;
    }

    let token;

    // Check a user is found
    if (user) {
      token = user.generateJwt();
      res.status(200);
      res.json({
        Token: token,
      });
    } else {
      res.status(401).json(info);
    }
  })(req, res);
};
