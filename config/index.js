const config = require('./config');

module.exports = {
  dbConnectionString: `mongodb://${config.db.username}:${config.db.password}@${config.db.host}`,
  io: {
    host: config.io.host,
    port: config.io.port,
  },
};
